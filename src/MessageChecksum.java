import java.io.*;
import java.util.*;

public class MessageChecksum {

    public String receiveMessage(int length, String s) {

        // Sender.getChecksum(0, 1)
        // Sender.getMessage(0, 1)

        int[] hist = new int[27];
        for (int i = 0; i < s.length(); i++) {
            hist[(int)s.charAt(i) - 65]++;
        }
        hist[26] = length - s.length();
        double rate = guessErrorRate(length, hist);

        if (rate < 0.02) {
            System.err.println("NON SOLVER");

            return s;

        } else if (rate < 0.30) {
            System.err.println("GUESS WITH EDIT-DISTANCE");

            return guessWithEditDistance(length, s, rate, hist);

        } else {
            System.err.println("BRUTE FORCE");

            return bruteForce(length, s);
        }

    }

    String guessWithEditDistance(int length, String s, double rate, int[] origHist) {
        int[] hist = new int[origHist.length];
        for (int i = 0; i < hist.length; i++) {
            hist[i] = origHist[i] * 100 + i;
        }
        Arrays.sort(hist);
        for (int i = 0; i < hist.length; i++) {
            hist[i] %= 100;
        }        
        
        int[] ss = new int[s.length()];
        for (int i = 0; i < ss.length; i++) {
            ss[i] = (int)s.charAt(i) - 65;
        }

        int[] tmp1 = null, tmp2 = null;

        for (int k = 0; k < 2; k++) {
            tmp1 = new int[length];
            if (tmp2 == null) {
                tmp2 = tmp1;
            }
            for (int i = 0; i < length; i++) {
                String t = Sender.getMessage(i, 1);
                if ("".equals(t)) {
                    tmp1[i] = -1;
                } else {
                    tmp1[i] = (int)t.charAt(0) - 65;
                }
            }
        }

        int[] sums = new int[length + 1];
        for (int i = 0; i < length; i++) {
            if (tmp1[i] != tmp2[i]) {
                tmp1[i] = -1;
                tmp2[i] = -1;
            }
            if (tmp1[i] < 0) {
                tmp1[i] = Sender.getChecksum(i, 1);
            }
            sums[i + 1] = (tmp1[i] + sums[i]) % 26;
        }

        int[] css = new int[length + 1];
        for (int i = 1; i < length; i++) {
            css[i] = -1;
        }
        css[length] = Sender.getChecksum(0, length);

        for (;;) {
            int sp = 0;
            if (css[length] == sums[length]) {
                for (int i = 1; i < length; i++) {
                    if (css[i] < 0) {
                        continue;
                    }
                    if (css[i] != sums[i]) {
                        // System.err.println("unmatch!");
                        sp = i;
                        break;
                    }
                }
                if (sp == 0 && rate >= 0.22) {
                    for (int i = 1; i < length; i++) {
                        if (tmp2[i] < 0 || tmp2[i - 1] < 0 || css[i] >= 0) {
                            i++;
                            continue;
                        }
                        if (!(tmp1[i] == hist[26]
                                || tmp1[i] == hist[25]
                                || tmp1[i] == hist[24])) {
                            continue;
                        }
                        fillCss(length, css, i, tmp1, tmp2);
                        if (css[i] != sums[i]) {
                            sp = i;
                            break;
                        }
                    }                    
                }
                if (sp == 0) {
                    break;
                }
            } else {
                sp = length;
            }

            int lp = 0, rp = sp;
            while (lp < rp) {
                int hp = (lp + rp) / 2;
                if (css[hp] < 0) {
                    fillCss(length, css, hp, tmp1, tmp2);
                }
                if (sums[hp] != css[hp]) {
                    rp = hp;
                } else {
                    lp = hp + 1;
                }
            }
            int e = tmp1[lp - 1];
            e = (e + css[lp] + 26 - sums[lp]) % 26;
            tmp1[lp - 1] = e;
            for (int i = lp - 1; i < length; i++) {
                sums[i + 1] = (tmp1[i] + sums[i]) % 26;
            }
        }

        char[] buf = new char[length];

        for (int i = 0; i < length; i++) {
            buf[i] = (char)(65 + tmp1[i]);
        }

        return String.valueOf(buf);
    }

    void fillCss(int length, int[] css, int p, int[] tmp1, int[] tmp2) {
        css[p] = Sender.getChecksum(0, p);
        int i = p;
        while (i + 1 < length && tmp2[i] < 0 && css[i + 1] < 0) {
            css[i + 1] = (css[i] + tmp1[i]) % 26;
            i++;
        }
        i = p;
        while (i > 0 && tmp2[i - 1] < 0 && css[i - 1] < 0) {
            css[i - 1] = (css[i] + 26 - tmp1[i - 1]) % 26;
            i--;
        }
    }

    String bruteForce(int length, String s) {
        int[] sums = new int[length];

        for (int i = 0; i < length - 1; i++) {
            sums[i] = Sender.getChecksum(i, 2);
        }

        int[] ss = new int[s.length()];
        for (int i = 0; i < ss.length; i++) {
            ss[i] = (int)s.charAt(i) - 65;
        }

        int sel = 0;
        int sc = Integer.MAX_VALUE;
        int checkLen = Math.min(100, length);
        int diffLen = Math.min(checkLen, s.length());

        for (int a = 0; a < 26; a++) {
            int[] tmp = new int[checkLen];
            tmp[0] = a;
            for (int i = 1; i < checkLen; i++) {
                tmp[i] = (sums[i - 1] + 26 - tmp[i - 1]) % 26;
            }
            int[][] dp = new int[2][diffLen + 1];
            for (int i = 0; i <= checkLen; i++) {
                int ii = i & 1;
                for (int j = 0; j <= diffLen; j++) {
                    if (i == 0) {
                        dp[ii][j] = j;
                    } else if (j == 0) {
                        dp[ii][j] = i;
                    } else if (tmp[i - 1] == ss[j - 1]) {
                        dp[ii][j] = dp[1 - ii][j - 1];
                    } else {
                        dp[ii][j] = 1 + Math.min(dp[1 - ii][j - 1], Math.min(dp[1 - ii][j], dp[ii][j - 1]));
                    }
                }
            }
            int tsc = dp[checkLen & 1][diffLen];
            if (tsc < sc) {
                sc = tsc;
                sel = a;
            }
        }

        char[] buf = new char[length];
        buf[0] = (char)(65 + sel);
        for (int i = 1; i < length; i++) {
            sel = (sums[i - 1] + 26 - sel) % 26;
            buf[i] = (char)(65 + sel);
        }

        return String.valueOf(buf);
    }

    double guessErrorRate(int length, int[] hist) {
        Random rand = new Random(19831983L);

        int[] tmp1 = Arrays.copyOf(hist, hist.length);
        Arrays.sort(tmp1);

        int[] ord = new int[25];
        int index = 0;

        for (int i = 1; i <= 50; i += 2) {

            double errorRate = (double)i / 100.0;

            int[] diffs = new int[20];
            for (int k = 0; k < diffs.length; k++) {
                int[] tmp2 = new int[27];
                for (int j = 0; j < length; j++) {
                    if (rand.nextDouble() < errorRate) {
                        tmp2[(int)Math.sqrt(Math.floor(rand.nextDouble() * 729.0))]++;
                    } else {
                        tmp2[rand.nextInt(26)]++;
                    }
                }
                Arrays.sort(tmp2);
                int diff = 0;
                for (int j = 0; j < tmp1.length; j++) {
                    diff += Math.abs(tmp1[j] - tmp2[j]);
                }
                diffs[k] = diff;
            }
            Arrays.sort(diffs);
            int omit = 5; //diffs.length * 15 / 100;
            for (int j = omit; j < diffs.length - omit; j++) {
                ord[index] += diffs[j];
            }
            ord[index] *= 100;
            ord[index] += i;

            index++;
        }

        Arrays.sort(ord);
        // int sum = 0;
        // for (int i = 0; i < 10; i++) {
            // sum += ord[i] % 100;
            // System.err.printf("No.%2d: dist 0.%02d %5d  ( %f )%n", i + 1,
                // ord[i] % 100, ord[i] / 100, (double)sum / (double)(i + 1) / 100.0);
        // }

        if ((double)(ord[1] / 100) / (double)(ord[0] / 100) > 1.2) {
            System.err.printf("guess errorRate %f !!%n", (double)(ord[0] % 100) / 100.0);
            return (double)(ord[0] % 100) / 100.0;
        }

        // double guessing0 = (double)sum / 1000.0;

        double guessing1 = (double)(ord[0] % 100) / 100.0;
        double guessing2 = (double)(ord[0] % 100 + ord[1] % 100) / 200.0;
        double guessing3 = (double)(ord[0] % 100 + ord[1] % 100 + ord[2] % 100) / 300.0;

        int[] rates = new int[5];
        for (int i = 0; i < 5; i++) {
            rates[i] = ord[i] % 100;
        }
        Arrays.sort(rates);
        double guessing4 = (double)(rates[2] + rates[3]) / 200.0;

        double guessing = Math.max(guessing1,
                          Math.max(guessing2,
                          Math.max(guessing3, guessing4)));

        // System.err.printf("guess errorRate %f !!%n", guessing0);
        // System.err.printf("guess errorRate %f !!%n", guessing1);
        // System.err.printf("guess errorRate %f !!%n", guessing2);
        // System.err.printf("guess errorRate %f !!%n", guessing3);
        // System.err.printf("guess errorRate %f !!%n", guessing4);
        System.err.printf("guess errorRate %f !!%n", guessing);

        return guessing;
    }

}
