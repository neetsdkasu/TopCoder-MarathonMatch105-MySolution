import java.io.*;
import java.util.*;

public class Main {

    static int callReceiveMessage(BufferedReader in, MessageChecksum messageChecksum) throws Exception {

        int length = Integer.parseInt(in.readLine());
        String s = in.readLine();

        String _result = messageChecksum.receiveMessage(length, s);

        System.out.println(_result);
        System.out.flush();

        return 0;
    }

    public static void main(String[] args) throws Exception {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        
        Sender.setInput(in);
        
        MessageChecksum messageChecksum = new MessageChecksum();

        // do edit codes if you need

        callReceiveMessage(in, messageChecksum);
        
        Sender.setInput(null);
    }

}

class Sender {
    
    private static BufferedReader in = null;
    
    static void setInput(BufferedReader in) {
        Sender.in = in;
    }
    
    private static String readLine() {
        try {
            return in.readLine();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public static int getChecksum(int start, int length) {
        System.out.println("?getChecksum");
        System.out.println(start);
        System.out.println(length);
        System.out.flush();
        return Integer.parseInt(readLine());
    }
    
    public static String getMessage(int start, int length) {
        System.out.println("?getMessage");
        System.out.println(start);
        System.out.println(length);
        System.out.flush();
        return readLine();
    }

}

