Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

    Public Sub Main()
        Try
            Dim _messageChecksum As New MessageChecksum()

            ' do edit codes if you need

            CallReceiveMessage(_messageChecksum)


        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

    Function CallReceiveMessage(_messageChecksum As MessageChecksum) As Integer
        Dim length As Integer = CInt(Console.ReadLine())
        Dim s As String = Console.ReadLine()

        Dim _result As String = _messageChecksum.receiveMessage(length, s)

        Console.WriteLine(_result)
        Console.Out.Flush()

        Return 0
    End Function

    Public Class Sender
        Public Shared Function getChecksum(start As Integer, length As Integer) As Integer
            Console.WriteLine("?getChecksum")
            Console.WriteLine(start)
            Console.WriteLine(length)
            Console.Out.Flush()
            Dim checksum As Integer = CInt(Console.ReadLine())
            Return checksum
        End Function

        Public Shared Function getMessage(start As Integer, length As Integer) As String
            Console.WriteLine("?getMessage")
            Console.WriteLine(start)
            Console.WriteLine(length)
            Console.Out.Flush()
            Dim message As String = Console.ReadLine()
            Return message
        End Function
    End Class

End Module