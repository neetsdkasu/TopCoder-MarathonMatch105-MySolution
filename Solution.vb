Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Integer, Integer)

Public Class MessageChecksum

    Dim rand As New Random(19831983)

    Public Function receiveMessage(length As Integer, s As String) As String

        ' Sender.getChecksum(0, 1)
        ' Sender.getMessage(0, 1)

        Dim hist(25) As Integer
        For i As Integer = 0 To s.Length - 1
            hist(Asc(s(i)) - 65) += 1
        Next i
        Dim xs(25) As Tp
        For i As Integer = 0 To UBound(hist)
            xs(i) = New Tp(hist(i), i)
        Next i
        Array.Sort(xs)
        Dim rate As Double = CDbl(xs(0).Item1) / CDbl(xs(25).Item1)
        Console.Error.WriteLine("rate {0}", rate)

        Dim buf(length - 1) As Char
        If rate > 0.622 Then
            Console.Error.WriteLine("GUESS 3 chars")
            For i As Integer = 0 To length - 1
                Dim t1 As String = Sender.getMessage(i, 1)
                Dim t2 As String = Sender.getMessage(i, 1)
                Dim t3 As String = Sender.getMessage(i, 1)
                If t1 <> "" AndAlso t1 = t2 AndAlso t2 = t3 Then
                    buf(i) = t1(0)
                Else
                    Dim u As Char = Chr(65 + Sender.getChecksum(i, 1))
                    buf(i) = u
                End If
            Next i
        ElseIf length < 5000 Then
            Console.Error.WriteLine("BRUTE FORCE")

            Dim sums(length - 2) As Integer
            For i As Integer = 0 To length - 2
                sums(i) = Sender.getChecksum(i, 2)
            Next i

            Dim ss(s.Length - 1) As Integer
            For i As Integer = 0 To UBound(ss)
                ss(i) = Asc(s(i)) - 65
            Next i

            Dim sel As Integer = 0
            Dim score As Integer = Integer.MaxValue
            For a As Integer = 0 To 25
                Dim tmp(length - 1) As Integer
                tmp(0) = a
                For i As Integer = 1 To UBound(tmp)
                    tmp(i) = (sums(i - 1) + 26 - tmp(i - 1)) Mod 26
                Next i
                Dim dp(1, ss.Length) As Integer
                For i As Integer = 0 To length
                    Dim ii As Integer = i And 1
                    For j As Integer = 0 To ss.Length
                        If i = 0 Then
                            dp(ii, j) = j
                        ElseIf j = 0 Then
                            dp(ii, j) = i
                        ElseIf tmp(i - 1) = ss(j - 1)
                            dp(ii, j) = dp(1 - ii, j - 1)
                        Else
                            dp(ii, j) = 1 + Math.Min(dp(1 - ii, j - 1), Math.Min(dp(1 - ii, j), dp(ii, j - 1)))
                        End If
                    Next j
                Next i
                Dim tmpscore As Integer = dp(length And 1, ss.Length)
                If tmpscore < score Then
                    score = tmpscore
                    sel = a
                End If
            Next a

            buf(0) = Chr(sel + 65)
            For i As Integer = 1 To length - 1
                sel = (sums(i - 1) + 26 - sel) Mod 26
                buf(i) = Chr(sel + 65)
            Next i
        Else
            Console.Error.WriteLine("NON SOLVER")
            For i As Integer = 0 To length - 1
                Dim u As Char = Chr(65 + Sender.getChecksum(i, 1))
                buf(i) = u
            Next i
        End If


        receiveMessage = New String(buf)

    End Function

End Class