@echo off
if "%~1"=="" goto defaultseed
java -Xms512M -jar tester.jar -exec "java -Xms512M -cp classes Main" -seed %*
@GOTO finally
:defaultseed
java -Xms512M -jar tester.jar -exec "java -Xms512M -cp classes Main" -seed 1
@GOTO finally
:finally
